﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using System.IO;

namespace SmartCAP.RTTI.Sample
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new RestClient("https://cloudia.smartcap-its.com");
            var request = new RestRequest("mobilesdk/api/ID", Method.POST);
            // enable image processing and yield full OCR details
            request.AddParameter("ProcessImage", true);
            request.AddParameter("OCRResults", true);
            // add the file that should be processed
            request.AddFile("Dowod2015.png", @"C:\Temp\Dowod2015.png", "image/png");


            // execute request and handle result
            IRestResponse response = client.Execute(request);
            var content = response.Content;

            // content will hold the JSON as string
            Console.WriteLine(content);
            Console.ReadKey();
        }
    }
}
